fun interface FileReader<T> {
    fun readingData(fileName: String): Iterable<T>
}