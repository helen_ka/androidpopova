fun main() {
    val inputFileName = "input.txt"
    val outputFileName = "output.txt"

    val fileManager = FileManager()
    val sortData = SortingData()
    val names = fileManager.readingData(inputFileName);
    val sortNames = sortData.sortLines(names);
    fileManager.writingData(outputFileName, sortNames)
}