package com.example.android3laba

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.android3laba.databinding.ActivitySecondBinding

class SecondActivity : AppCompatActivity() {

    private lateinit var binding: ActivitySecondBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySecondBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        initViews()
    }

    private fun initViews() {
        val myText = intent.getStringExtra(KEY_TEXT) ?: "hello"
        binding.tvInfo.text = myText
    }

}